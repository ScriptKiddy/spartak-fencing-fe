'use strict';


var DesignLayout = (function($, Logger, Page, ScreenHelper, BootstrapModal, Utils) {
    var module = {};

    var settings = {
        moduleName: 'DesignLayout',
        debug: true,
        edit_mode: false,
    };

    var ui_selectors = {
        // main blocks
        body: 'body',
        page: '.page',
        main: 'main',
        mainContent: '.page__content',
        fixedHeader: '.fixed-header',

        // overlays
        overlayBody: '.page__overlay',

        //panel
        panel: '.page__panel',
        // buttons for panelz
        panelOpen: '#panelOpen',
        panelClose: '#panelClose',

        // modals registration 
        fullWidthElements: '.page__content, .fixed-header',
        positionedElements: '',

        // map
        map: '#map',

        // popovers in schedule
        //schedulePopovers: '.schedule .item',

        // radio on order halls
        orderHallsRadios: '.order__form .form input[type="radio"]',

        // time interval in orders pages
        sliderPeriod: '.slider-block .slider',

        // tabs in account
        navs: '.account .nav-tabs > li > a',

        // orders in account
        orders: '.order .order__header'
    };

    var ui = {};

    var logger;

    var useCSS3Animation;
    var animationDurationMS = Page.getAnimationSpeed();
    var animationDuration = animationDurationMS / 1000;

    var scrollbarWidth;

    var windowLoad = false;

    function initPhotoSwipeFromDOM(gallerySelector) {

      // parse slide data (url, title, size ...) from DOM elements
      // (children of gallerySelector)
      var parseThumbnailElements = function(el) {
        var thumbElements = $(el).find('.photoswipe-item:not(.isotope-hidden)').get(),
          numNodes = thumbElements.length,
          items = [],
          figureEl,
          linkEl,
          size,
          item;

        for (var i = 0; i < numNodes; i++) {

          figureEl = thumbElements[i]; // <figure> element

          // include only element nodes
          if (figureEl.nodeType !== 1) {
            continue;
          }

          linkEl = figureEl.children[0]; // <a> element

          size = linkEl.getAttribute('data-size').split('x');

          // create slide object
          if ($(linkEl).data('type') == 'video') {
            item = {
              html: $(linkEl).data('video')
            };
          } else {
            item = {
              src: linkEl.getAttribute('href'),
              w: parseInt(size[0], 10),
              h: parseInt(size[1], 10)
            };
          }

          if (figureEl.children.length > 1) {
            // <figcaption> content
            item.title = $(figureEl).find('.caption').html();
          }

          if (linkEl.children.length > 0) {
            // <img> thumbnail element, retrieving thumbnail url
            item.msrc = linkEl.children[0].getAttribute('src');
          }

          item.el = figureEl; // save link to element for getThumbBoundsFn
          items.push(item);
        }

        return items;
      };

      // find nearest parent element
      var closest = function closest(el, fn) {
        return el && (fn(el) ? el : closest(el.parentNode, fn));
      };

      function hasClass(element, cls) {
        return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
      }

      // triggers when user clicks on thumbnail
      var onThumbnailsClick = function(e) {
        e = e || window.event;
        e.preventDefault ? e.preventDefault() : e.returnValue = false;

        var eTarget = e.target || e.srcElement;

        // find root element of slide
        var clickedListItem = closest(eTarget, function(el) {
          return (hasClass(el, 'photoswipe-item'));
        });

        if (!clickedListItem) {
          return;
        }

        // find index of clicked item by looping through all child nodes
        // alternatively, you may define index via data- attribute
        var clickedGallery = clickedListItem.closest('.photoswipe-wrapper'),
          childNodes = $(clickedListItem.closest('.photoswipe-wrapper')).find('.photoswipe-item:not(.isotope-hidden)').get(),
          numChildNodes = childNodes.length,
          nodeIndex = 0,
          index;

        for (var i = 0; i < numChildNodes; i++) {
          if (childNodes[i].nodeType !== 1) {
            continue;
          }

          if (childNodes[i] === clickedListItem) {
            index = nodeIndex;
            break;
          }
          nodeIndex++;
        }

        if (index >= 0) {
          // open PhotoSwipe if valid index found
          openPhotoSwipe(index, clickedGallery);
        }
        return false;
      };

      // parse picture index and gallery index from URL (#&pid=1&gid=2)
      var photoswipeParseHash = function() {
        var hash = window.location.hash.substring(1),
          params = {};

        if (hash.length < 5) {
          return params;
        }

        var vars = hash.split('&');
        for (var i = 0; i < vars.length; i++) {
          if (!vars[i]) {
            continue;
          }
          var pair = vars[i].split('=');
          if (pair.length < 2) {
            continue;
          }
          params[pair[0]] = pair[1];
        }

        if (params.gid) {
          params.gid = parseInt(params.gid, 10);
        }

        return params;
      };

      var openPhotoSwipe = function(index, galleryElement, disableAnimation, fromURL) {
        var pswpElement = document.querySelectorAll('.pswp')[0],
          gallery,
          options,
          items;

        items = parseThumbnailElements(galleryElement);

        // define options (if needed)
        options = {
          
          closeOnScroll: false,
          
          // define gallery index (for URL)
          galleryUID: galleryElement.getAttribute('data-pswp-uid'),

          getThumbBoundsFn: function(index) {
            // See Options -> getThumbBoundsFn section of documentation for more info
            var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
              pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
              rect = thumbnail.getBoundingClientRect();

            return {
              x: rect.left,
              y: rect.top + pageYScroll,
              w: rect.width
            };
          }

        };

        // PhotoSwipe opened from URL
        if (fromURL) {
          if (options.galleryPIDs) {
            // parse real index when custom PIDs are used
            // http://photoswipe.com/documentation/faq.html#custom-pid-in-url
            for (var j = 0; j < items.length; j++) {
              if (items[j].pid == index) {
                options.index = j;
                break;
              }
            }
          } else {
            // in URL indexes start from 1
            options.index = parseInt(index, 10) - 1;
          }
        } else {
          options.index = parseInt(index, 10);
        }

        // exit if index not found
        if (isNaN(options.index)) {
          return;
        }

        if (disableAnimation) {
          options.showAnimationDuration = 0;
        }

        // Pass data to PhotoSwipe and initialize it
        gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
        gallery.init();
    
        //iframe class = .pswp__video 
        /*
        gallery.listen('beforeChange', function() {
          var currItem = $(gallery.currItem.container);
          $('.pswp__video').removeClass('active');
          var currItemIframe = currItem.find('.pswp__video').addClass('active');
          $('.pswp__video').each(function() {
            if (!$(this).hasClass('active')) {
              $(this).attr('src', $(this).attr('src'));
            }
          });
        });

        gallery.listen('close', function() {
          $('.pswp__video').each(function() {
            $(this).attr('src', $(this).attr('src'));
          });
        });
        */

        //Iframe without class
        gallery.listen('beforeChange', function() {
          var currItem = $(gallery.currItem.container);
          $('iframe').removeClass('active');
          var currItemIframe = currItem.find('iframe').addClass('active');
          $('iframe').each(function() {
            if (!$(this).hasClass('active')) {
              $(this).attr('src', $(this).attr('src'));
            }
          });
        });

        gallery.listen('close', function() {
          $('iframe').each(function() {
            $(this).attr('src', $(this).attr('src'));
          });
        });
      };

      // loop through all gallery elements and bind events
      var galleryElements = document.querySelectorAll(gallerySelector);

      for (var i = 0, l = galleryElements.length; i < l; i++) {
        galleryElements[i].setAttribute('data-pswp-uid', i + 1);
        galleryElements[i].onclick = onThumbnailsClick;
      }

      // Parse URL and open gallery if it contains #&pid=3&gid=1
      var hashData = photoswipeParseHash();
      if (hashData.pid && hashData.gid) {
        openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], true, true);
      }

    };


    function rebuildUI() {
       for (var pr in ui_selectors) {
            ui[pr] = $(ui_selectors[pr]);
        }
    }

    function ieVersion() {
        var ua = window.navigator.userAgent;
        var msie = ua.indexOf('MSIE');
        if (msie > 0) {
            return parseInt (ua.substring(msie + 5, ua.indexOf('.', msie)));
        }
        if (ua.indexOf('Trident/7.0') + 1) {
            return 11;
        }
        return 0;
    }

    function checkScroll() {
        var scroll;
        if (!(ieVersion() && ieVersion())) {
            scroll = $(window).scrollTop();
        } else {
            scroll = document.documentElement.scrollTop;
        }

        if (document.documentElement.clientWidth >= ScreenHelper.screenSM - scrollbarWidth
            && document.documentElement.clientWidth < ScreenHelper.screenMD - scrollbarWidth) {
            if (scroll > 170) {
                if (!ui.fixedHeader.hasClass('scroll')) {
                    ui.fixedHeader.addClass('scroll');
                    ui.main.css('padding-top', 250);
                }
                if (!ui.body.hasClass('panel-open')) {
                    ui.panelOpen.css({opacity: 1});
                }
            } else {
                ui.main.css('padding-top', '');
                if (ui.fixedHeader.hasClass('scroll')) {
                    ui.fixedHeader.removeClass('scroll');
                }
                ui.panelOpen.css({opacity: 0});
                if (ui.body.hasClass('panel-open')) {
                    closePanel();
                }
            }
        } else if (document.documentElement.clientWidth >= ScreenHelper.screenMD - scrollbarWidth) {
            if (scroll > 170) {
                if (!ui.fixedHeader.hasClass('scroll')) {
                    ui.fixedHeader.addClass('scroll');
                    ui.main.css('padding-top', 250);
                }
            } else {
                ui.main.css('padding-top', '');
                if (ui.fixedHeader.hasClass('scroll')) {
                    ui.fixedHeader.removeClass('scroll');
                }
            }
        } else {
            ui.fixedHeader.removeClass('scroll');
            ui.main.css('padding-top', '');
        }
    }

    // set overlay height
    function setOverlayHeight() {
        if (ui.body.height() > document.documentElement.clientHeight) {
            ui.overlayBody.css({
                display: 'block',
                height: ui.body.height()
            });
        } else {
            ui.overlayBody.css({
                display: 'block',
                height: document.documentElement.clientHeight
            });
        }
    }

    // left menu open
    function openPanel() {
        ui.body.addClass('panel-open');

        setOverlayHeight();

        ui.fixedHeader.css({'z-index': '1150'});
        ui.panelClose.css('display', 'block');

        setPanelTransition(animationDuration);

        if (useCSS3Animation) {
            ui.overlayBody.css({opacity: 1});
            if (document.documentElement.clientWidth >= ScreenHelper.screenSM - ScreenHelper.getScrollbarWidth()) {
                Utils.css3(ui.panel, {'%stransform': 'translate(320px, 0px)'});
            } else {
                Utils.css3(ui.panel, {'%stransform': 'translate(100%, 0px)'});
            }
            ui.panelOpen.css({opacity: 0});
            ui.panelClose.css({opacity: 1});
        } else {
            ui.overlayBody.animate({opacity: 1}, {duration: animationDurationMS, queue: false});
            ui.panel.animate({left: '0px'}, {duration: animationDurationMS, queue: false});
            ui.panelOpen.animate({ opacity: 0}, {duration: animationDurationMS, queue: false});
            ui.panelClose.animate({opacity: 1}, {duration: animationDurationMS, queue: false});
        }

    }

    // left menu close
    function closePanel() {
        ui.body.removeClass('panel-open');

        setPanelTransition(animationDuration);

        if (useCSS3Animation) {
            ui.overlayBody.css({opacity: 0});
            window.setTimeout(function() {
                ui.overlayBody.hide();
                ui.fixedHeader.css({'z-index': ''});
                ui.panelClose.hide();
            }, animationDurationMS);
            Utils.css3(ui.panel, {'%stransform': 'translate(0px, 0px)'});
            ui.panelClose.css({opacity: 0});
            if (document.documentElement.clientWidth >= ScreenHelper.screenSM - ScreenHelper.getScrollbarWidth()
                && document.documentElement.clientWidth < ScreenHelper.screenMD - ScreenHelper.getScrollbarWidth()) {
                if (ui.fixedHeader.hasClass('scroll')) {
                    ui.panelOpen.css({opacity: 1});
                } else {
                    ui.panelOpen.css({opacity: 0});
                }
            } else {
                ui.panelOpen.css({opacity: 1});
            }
        } else {
            ui.overlayBody.animate({
                opacity: 0
            }, {
                duration: animationDurationMS,
                queue: false,
                complete: function() {
                    ui.overlayBody.hide();
                    ui.fixedHeader.css({'z-index': ''});
                    ui.panelClose.hide();
                }
            });
            if (document.documentElement.clientWidth >= ScreenHelper.screenSM - ScreenHelper.getScrollbarWidth()) {
                ui.panel.animate({left: '-320px'}, {duration: animationDurationMS, queue: false});
            } else {
                ui.panel.animate({left: '-100%'}, {duration: animationDurationMS, queue: false});
            }
            ui.panelClose.animate({opacity: 0}, {duration: animationDurationMS, queue: false});
            if (document.documentElement.clientWidth >= ScreenHelper.screenSM - ScreenHelper.getScrollbarWidth()
                && document.documentElement.clientWidth < ScreenHelper.screenLG - ScreenHelper.getScrollbarWidth()) {
                if (ui.fixedHeader.hasClass('scroll')) {
                    ui.panelOpen.animate({ opacity: 1}, {duration: animationDurationMS, queue: false});
                } else {
                    ui.panelOpen.animate({ opacity: 0}, {duration: animationDurationMS, queue: false});
                }
            } else {
                ui.panelOpen.animate({ opacity: 1}, {duration: animationDurationMS, queue: false});
            }
        }
    }

    // Проверка панели если требуется
    function checkPanel() {

        if (ui.body.hasClass('panel-open')) {

            setPanelTransition(0);

            if (useCSS3Animation) {
                ui.overlayBody.css({opacity: 0}).hide();
                Utils.css3(ui.panel, {'%stransform': 'translate(0px, 0px)'});
                ui.panelClose.css({opacity: 0}).hide();
                ui.panelOpen.css({opacity: 1});
            } else {
                ui.panel.css({left: '0'});
                ui.overlayBody.css('opacity', 0).hide();
                ui.panelClose.css('opacity', 0).hide();
                ui.panelOpen.css('opacity', 1);
            }
            ui.fixedHeader.css({'z-index': ''});

            ui.body.removeClass('panel-open');
        }

        if (document.documentElement.clientWidth < ScreenHelper.screenSM - ScreenHelper.getScrollbarWidth()) {
            if (useCSS3Animation) {
                ui.panelOpen.css({opacity: 1});
            } else {
                ui.panelOpen.css('opacity', 1);
            }
        } else if (document.documentElement.clientWidth >= ScreenHelper.screenSM - ScreenHelper.getScrollbarWidth()
            && document.documentElement.clientWidth < ScreenHelper.screenMD - ScreenHelper.getScrollbarWidth()) {
            if (useCSS3Animation) {
                if (ui.fixedHeader.hasClass('scroll')) {
                    ui.panelOpen.css({opacity: 1});
                } else {
                    ui.panelOpen.css({opacity: 0});
                }
            } else {
                if (ui.fixedHeader.hasClass('scroll')) {
                    ui.panelOpen.animate({ opacity: 1}, {duration: animationDurationMS, queue: false});
                } else {
                    ui.panelOpen.animate({ opacity: 0}, {duration: animationDurationMS, queue: false});
                }
            }
        } else {
            if (useCSS3Animation) {
                ui.panelOpen.css({opacity: 0});
            } else {
                ui.panelOpen.animate({ opacity: 0}, {duration: animationDurationMS, queue: false});
            }
        }
    }

    var pageInitCount = 0;
    function initPanels() {
        if (pageInitCount++ > 0) return;
        // Второй вариант скриптов для контроля панелей
        $(document).on('click', function(evt) {
            var $triggerOnPanel = $(evt.target).closest('.page__panel');
            var $triggerOnPanelLink = $(evt.target).closest('.page__panel nav ul > li a');
            var $triggerOpenPanel = $(evt.target).closest('#panelOpen');
            var $triggerClosePanel = $(evt.target).closest('#panelClose');

            if (ui.body.hasClass('panel-open')) {

                if (!$triggerOnPanelLink) {
                    evt.preventDefault();
                }

                if (!$triggerOnPanel.length || $triggerClosePanel.length) {
                    evt.preventDefault();
                    closePanel();
                }

            } else {

                if ($triggerOpenPanel.length) {
                    evt.preventDefault();
                    openPanel();
                }

            }
        });
    }

    var touchEventsInitCount = 0;
    function initTouchEvents() {
        if (touchEventsInitCount++ > 0) return;
        // Фиксация клика для сенсорных устройств
        /*$(document).on('touchend', function(evt){
            evt.preventDefault();

            var $trigger = $(evt.target);

            evt.target.click();

            if ($trigger.hasClass('form-control')) {
                $trigger.focus();
            }
        });*/
    }

    function setPanelTransition(duration) {
        if (useCSS3Animation) {
            Utils.css3(ui.panel, {'%stransition': '%stransform ' + duration + 's'});
            Utils.css3(ui.overlayBody, {'%stransition': 'opacity ' + duration + 's'});
            Utils.css3(ui.panelOpen, {'%stransition': 'opacity ' + duration + 's'});
            Utils.css3(ui.panelClose, {'%stransition': 'opacity ' + duration + 's'});
        }
    }

    // Основной пересчёт/перерисовка
    function redraw() {
        logger.log('redraw');

        // Высота страницы
        ui.mainContent.innerHeight('auto');
        if (ui.mainContent.innerHeight() < document.documentElement.clientHeight) {
            ui.mainContent.innerHeight(document.documentElement.clientHeight);
        }

        checkPanel();
    }

    module.redraw = redraw;
    module.rebuildUI = rebuildUI;

    function init() {
        $(document).ready(function() {
            logger.log('event: DOM ready');

            // prepare jquery ui objects
            rebuildUI();

            if(navigator.userAgent.indexOf('Mac') > 0) {
                $('body').addClass('mac-os');
            }

            checkScroll();

            initTouchEvents();
            initPanels();

            // Не закрывать дропдауны с .keep-open при клике внутри!!!
            $(document).on('click', '.dropdown-menu', function (evt) {
                $(this).hasClass('keep-open') && evt.stopPropagation();
            });

            // Закрываем панель при открытии дропдаунов (в хедере есть 2)
            $('.dropdown').on('show.bs.dropdown', function () {
                if (ui.body.hasClass('panel-open')) {
                    closePanel();
                }
            });

            // init module
            BootstrapModal.init({
                debug: debug
            });

            // modals registration

            // modal Template
            BootstrapModal.register($('#modal-template'), $('.modal-template-trigger'),
                                    $(ui.fullWidthElements), $(ui.positionedElements));

            // modal Auth
            BootstrapModal.register($('#modal-auth'), $('.modal-auth-trigger'),
                                    $(ui.fullWidthElements), $(ui.positionedElements));

            // modal Checkout
            BootstrapModal.register($('#modal-checkout'), $('.modal-checkout-trigger'),
                                    $(ui.fullWidthElements), $(ui.positionedElements));

            // modal Checkout
            BootstrapModal.register($('#modal-call'), $('.modal-call-trigger'),
                                    $(ui.fullWidthElements), $(ui.positionedElements));

            $('.modal').on('shown.bs.modal', function () {
                if (document.documentElement.clientWidth >= ScreenHelper.screenSM - ScreenHelper.getScrollbarWidth()) {
                    $(this).find('.modal-content').css('margin-top', (document.documentElement.clientHeight - $(this).find('.modal-content').height()) / 2.25);
                } else {
                    $(this).find('.modal-content').css('margin-top', '');
                }
            });

            // Карта
            if (ui.map.length) {
                var coordinatesMap,
                    coordinatesMarker = {lat: 59.946360, lng: 30.361096},
                    markerImage = '/static/img/mapmarker.png',
                    zoom = 15;

                    if (document.documentElement.clientWidth >= ScreenHelper.screenSM - ScreenHelper.getScrollbarWidth()) {
                        coordinatesMap = {lat: 59.946948, lng: 30.358723};
                    } else {
                        coordinatesMap = coordinatesMarker;
                    }

                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: coordinatesMap,
                        zoom: zoom,
                        disableDefaultUI: true,
                        scrollwheel: false
                    }),

                    marker = new google.maps.Marker({
                        position: coordinatesMarker,
                        map: map,
                        icon: markerImage,
                        animation: google.maps.Animation.BOUNCE
                    });
            }

            
            // Photoswipe
            module.initPhotoswipe();

            // Слайдеры в Клубе
            if (ui.page.hasClass('club')) {
                var sliderCoaches = new Swiper('.swiper-container-coaches', {
                    speed: 300,
                    autoplay: 10000,
                    loop: settings.edit_mode ? 0 : 1,
                    spaceBetween: 0,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    pagination: '.swiper-pagination',
                    paginationClickable: true,
                    paginationType: 'bullets'
                });

                var sliderSportsmans = new Swiper('.swiper-container-sportsmans', {
                    speed: 300,
                    autoplay: 10000,
                    loop: settings.edit_mode ? 0 : 1,
                    slidesPerView: 3,
                    spaceBetween: 0,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    pagination: '.swiper-pagination',
                    paginationClickable: true,
                    paginationType: 'bullets',
                    breakpoints: {
                    // when window width is <= 320px
                    1060: {
                      slidesPerView: 1,
                      spaceBetween: 0
                    }
                  }
                });

                var sliderCamp = new Swiper('.swiper-container-camp', {
                    slidesPerView: 'auto',
                    centeredSlides: false,
                    loop: settings.edit_mode ? 0 : 1,
                    spaceBetween: 0,
                    nextButton: '.swiper-button-next',
                    prevButton: '.swiper-button-prev',
                    pagination: '.swiper-pagination',
                    paginationClickable: true,
                    paginationType: 'bullets'
                });
            }
            
            // Заказы в акаунте
            if (ui.page.hasClass('account')) {
                ui.orders.on('click', function(evt) {
                    evt.preventDefault();
                    if ($(this).closest('.order').hasClass('order--open')) {
                        $(this).closest('.order').removeClass('order--open');
                    } else {
                        $(this).closest('.order').addClass('order--open');
                    }
                });
            }

            redraw();
        });

        $(window).load(function() {
            logger.log('event: window load');

            windowLoad = true;

            redraw();
        });

        $(window).scroll(function() {
            logger.log('event: window scroll');

            // теоретически событие может наступить до того, как будет готова DOM модель,
            // поэтому, чтобы избежать ошибок, используем здесь $(ui.el) вместо ui.el

            if (windowLoad) {
                checkScroll();
            }
        });

        $(window).resize(function() {
            logger.log('event: window resize');

            checkPanel();
            checkScroll();

            redraw();
        });
    }

    module.initPhotoswipe = function(){
        if ($('.photoswipe-wrapper').length) {
            $('.photoswipe-wrapper').imagesLoaded(function(){
                $('.photoswipe-wrapper').each(function() {
                  $(this).find('a').each(function() {
                    if( $(this).find('img').length )
                        $(this).attr('data-size', $(this).find('img').get(0).naturalWidth + 'x' + $(this).find('img').get(0).naturalHeight);
                  });
                });
                // execute above function
                initPhotoSwipeFromDOM('.photoswipe-wrapper');
            });
        }
    }

    module.init = function(_settings) {
        _settings = _settings || {};

        for (var pr in _settings) {
            settings[pr] = _settings[pr];
        }

        logger = new Logger(settings);

        useCSS3Animation = Utils.supportsCSSProp('transition') 
                           && Utils.supportsCSSProp('transform');

        scrollbarWidth = ScreenHelper.getScrollbarWidth();

        init();

        logger.log('init');
    };

    return module;
}(jQuery, Logger, Page, ScreenHelper, BootstrapModal, Utils));
